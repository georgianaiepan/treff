import React, { Component } from "react";
import {
    MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBFooter,
  MDBNavbarToggler,
} from "mdbreact";
import { ReactComponent as Logo } from "./assets/logo.svg";
import { BrowserRouter as Router, Route  } from "react-router-dom";
import Routes from "./Routes";
import "./Homepage.css";
import LoginPage from "./components/login";
import RegisterPage from "./components/register";
import withFirebaseAuth from 'react-with-firebase-auth'
import * as firebase from 'firebase/app';
import 'firebase/auth';
import firebaseConfig from './firebaseConfig';
import Harta from "./components/map";
import logo from "./trefflogo.png";

const firebaseApp = firebase.initializeApp(firebaseConfig);

const firebaseAppAuth = firebaseApp.auth();const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider(),
};

class Homepage extends Component {
    state = {
        collapseID: ""
      };
      
     

      toggleCollapse = collapseID => () =>
        this.setState(prevState => ({
          collapseID: prevState.collapseID !== collapseID ? collapseID : ""
        }));
    
      closeCollapse = collapseID => () => {
        window.scrollTo(0, 0);
        this.state.collapseID === collapseID && this.setState({ collapseID: "" });
      };
    
      render() {
        const overlay = (
          <div
            id="sidenav-overlay"
            style={{ backgroundColor: "transparent" }}
            onClick={this.toggleCollapse("mainNavbarCollapse")}
          />
        );
    
        const { collapseID } = this.state;

    return (
        <Router>
            <div className="flyout">
                <MDBNavbar color="green" dark expand="md" fixed="top" scrolling>
                    <MDBNavbarBrand href="/" className="py-0 font-weight-bold">
                        <Logo style={{ height: "2.5rem", width: "2.5rem" }} />
                        <strong className="align-middle">Treff</strong>
                    </MDBNavbarBrand>
                      <MDBNavbarNav right>
                           <MDBNavItem>               
                              {
                            
                                    this.props.user 
                                  ? <p>Hello, {this.props.user.displayName}</p>
                                  : null
                              }
                            </MDBNavItem>
                        <MDBNavItem>
                                  {this.props.user
                          ? <MDBNavLink onClick={this.props.signOut}>Sign out</MDBNavLink>
                          : <MDBNavLink onClick={this.props.signInWithGoogle}>Sign in with Google</MDBNavLink>
                                  }
                        </MDBNavItem>
                        
                      </MDBNavbarNav>
                  </MDBNavbar>
                  {
                    this.props.user
                    ? <Harta /> : 
                    <div className="mainDiv">
                      <div className="logoDiv">
                        <img src={logo} className="logo" alt="logo" />
                      </div>
                      <div className="descriptionDiv">
                        <p className="textDiv">
                          Treff este o aplicatie online ce iti arata in timp real eventimente din jurul tau. Pe harta interactiva poti vedea petreceri, concerte si alte evenimente la care ti-ar placea sa participi impreuna cu prietenii. Aplicatia este perfecta pentru persoanele sociabile care ies in oras cu gandul sa se distreze, oferindu-le locul perfect pentru genul de activitate pe care si-l doresc in acel moment.
                        </p>
                      </div>
                    </div>
                 }
                  <MDBFooter color="grey">
                    <p  style={{ textAlign:"center" }}>
                      artIsTs, DPIT 2019
                    </p>
                  </MDBFooter>
                  
              </div>
          </Router>
    )
}

}

export default withFirebaseAuth({
  providers,
  firebaseAppAuth,
})(Homepage);
