import React from "react";
import { MDBCard, MDBCardTitle, MDBContainer, MDBInput, MDBBtn } from 'mdbreact';

const LoginPage = () => {
  return (
    <MDBContainer>
      <MDBCard className="card-body" style={{ width: "22rem", marginTop: "1rem" }}>
          <MDBCardTitle>Login</MDBCardTitle>

          <form>
            <div className="grey-text">
              <MDBInput
                label="Type your email"
                icon="envelope"
                group
                type="email"
                validate
                error="wrong"
                success="right"
              />
              <MDBInput
                label="Type your password"
                icon="lock"
                group
                type="password"
                validate
              />
            </div>
            <div className="text-center">
              <MDBBtn>Login</MDBBtn>
            </div>
          </form>
        </MDBCard>
      </MDBContainer>
  );
};

export default LoginPage;