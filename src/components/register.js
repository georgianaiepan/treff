import React from "react";
import { MDBCard, MDBCardTitle, MDBContainer, MDBInput, MDBBtn } from 'mdbreact';

const RegisterPage = () => {
  return (
    <MDBContainer>
    <MDBCard className="card-body" style={{ width: "22rem", marginTop: "1rem" }}>
        <MDBCardTitle>Register</MDBCardTitle>
          <form>
            <p className="h5 text-center mb-4">Sign up</p>
            <div className="grey-text">
              <MDBInput
                label="Your name"
                icon="user"
                group
                type="text"
                validate
                error="wrong"
                success="right"
              />
              <MDBInput
                label="Your email"
                icon="envelope"
                group
                type="email"
                validate
                error="wrong"
                success="right"
              />
              <MDBInput
                label="Your password"
                icon="lock"
                group
                type="password"
                validate
              />
            </div>
            <div className="text-center">
              <MDBBtn color="primary">Register</MDBBtn>
            </div>
          </form>
          </MDBCard>
      </MDBContainer>
  );
};

export default RegisterPage;